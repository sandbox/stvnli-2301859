<?php

/**
 * @file
 * Fixed menus that will slide out from the sides of the page and in case of the right and left side optionally move the body. Admin has ability to choose any menu block to display on the Fixed menus.
 */

/**
 * Implements hook_permission().
 */
function slide_push_permission() {
  return array(
    'administer slide push menu' => array(
      'title' => t('Administer slide push menu'),
      'description' => t('Perform administration tasks for slide push menu.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function slide_push_menu() {
  $items['admin/config/user-interface/slide-push'] = array(
    'title' => 'Slide Push Menu',
    'page callback' => 'slide_push_settings',
    'access arguments' => array('administer slide push menu'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}


/**
 * Implements hook_theme().
 */
function slide_push_theme($existing, $type, $theme, $path) {
  return array(
    'slide_push_settings_form' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Create setting page.
 */
function slide_push_settings() {
  return drupal_get_form('slide_push_settings_form');
}

/**
 * Create setting form.
 *
 * @param array $form
 *   Standard form array
 *
 * @param array $form_state
 *   Standard form_state array
 *
 * @return array $form
 */
function slide_push_settings_form($form, &$form_state) {

  $form['#tree'] = TRUE;

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide Push Menu'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['settings']['screenshot'] = array(
    '#type' => 'item',
    '#title' => 'Screen shot',
    '#markup' => '<div id="screenshot-slidepush-wrapper"  class="clearfix">
                  <img src="' . url(drupal_get_path('module', 'slide_push') . '/images/screenshot-slidepush.png') . '"></div>',
  );

  $form['settings']['slide_push_menu_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label of menu button'),
    '#default_value' => variable_get("slide_push_menu_label", 'Menu'),
    '#description' => t('The clickable text which activates the menu.'),
    '#required' => TRUE,
  );

  $form['settings']['slide_push_header_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label of menu header'),
    '#default_value' => variable_get("slide_push_header_label", 'Menu'),
    '#description' => t('The text "heading" at the top of the menu.'),
    '#required' => TRUE,
  );

  $form['settings']['slide_push_close_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label of close button'),
    '#default_value' => variable_get("slide_push_close_label", 'X'),
    '#description' => t('The clickable text which closes the menu.'),
    '#required' => TRUE,
  );

  $form['settings']['slide_push_menu_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu button location'),
    '#default_value' => variable_get("slide_push_menu_class", '#region-branding .branding-data'),
    '#description' => t('Please specify the class/id of the HTML element to
                         which the menu button should be prepended.'),
    '#required' => TRUE,
  );

  $form['settings']['slide_push_menu_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu width'),
    '#default_value' => variable_get("slide_push_menu_width", '22.625em'),
    '#description' => t('Please specify width of the menu in em or px.
                          e.g.: 22.625em or 240px'),
    '#required' => TRUE,
  );

  $form['settings']['slide_push_side'] = array(
    '#type' => 'select',
    '#title' => t('Menu Location'),
    '#options' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
    '#default_value' => variable_get("slide_push_side", "left"),
    '#description' => t('Which side of the screen the menu will be placed.'),
    '#required' => TRUE,
  );

  $active = array(
    0 => t('Overlay'),
    1 => t('Push'),
  );

  $form['settings']['slide_push_is_push'] = array(
    '#type' => 'radios',
    '#title' => t('Menu Behavior'),
    '#default_value' => variable_get("slide_push_is_push", 0),
    '#options' => $active,
    '#required' => FALSE,
    '#description' => t('Overlay, will cause the menu to be overlaid on top
        of all content. Push, will push all content to the side of the screen
        opposite to the menu.'),
    '#prefix' => '<div id="slidepush-is-push-wrapper">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'callback' => 'slide_push_is_push_ajax_callback',
      'wrapper' => 'slidepush-push-class-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  if (variable_get("slide_push_is_push", 0)) {
    $form['settings']['slide_push_push_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Push Class'),
      '#default_value' => variable_get("slide_push_push_class", '#page'),
      '#description' => t('Please specify the class/id of the content,
                            which will be pushed. e.g.: #page'),
      '#required' => TRUE,
      '#prefix' => '<div id="slidepush-push-class-wrapper">',
      '#suffix' => '</div>',
      '#access' => variable_get("slide_push_is_push", 0),
    );
  }
  else {
    $form['settings']['is_push_placeholder'] = array(
      '#type' => 'item',
      '#markup' => '<div id="slidepush-push-class-wrapper"></div>',
    );
  }

  $menu_blocks = slide_push_sorted_menu_blocks();

  if (!empty($menu_blocks)) {
    foreach ($menu_blocks as $i => $block) {
      $bid = $block['bid'];

      if ($block['enabled']) {
        if (empty($form['settings']['slide_push_menu_blocks'])) {
          $form['settings']['slide_push_menu_blocks'] = array(
            '#type' => 'fieldset',
            '#title' => t('Menu Blocks'),
            '#collapsible' => FALSE,
            '#collapsed' => FALSE,
          );
        }

        /**
         * This block used to have no title. it is not worthy to
         * load the block info
         */
        $menu_blocks_info = block_load('menu_block', $bid);
        $menu_blocks_config = menu_block_get_config($bid);

        $block_is_active = !empty($menu_blocks_info->theme) && !empty($menu_blocks_info->region) && !empty($menu_blocks_info->status);
        if (!$block_is_active) {
          continue;
        }

        // hidden field to hold block id.
        $form['settings']['slide_push_menu_blocks'][$bid]['bid'] = array(
          '#type' => 'hidden',
          '#value' => $bid,
        );

        $form['settings']['slide_push_menu_blocks'][$bid]['enabled'] = array(
          '#type' => 'hidden',
          '#value' => 1,
        );

        $menu_blocks_title = l($menu_blocks_config['admin_title'] . ' [bid:' . $bid . ']', 'admin/structure/block/manage/menu_block/' . $bid . '/configure', array('attributes' => array('target' => '_blank')));

        // markup field to display block title
        $form['settings']['slide_push_menu_blocks'][$bid]['title'] = array(
          '#type' => 'item',
          '#markup' => $menu_blocks_title,
        );

        // This field is invisible, but contains sort info (weights).
        $form['settings']['slide_push_menu_blocks'][$bid]['weight'] = array(
          '#type' => 'weight',
          '#title' => t('Weight'),
          '#title_display' => 'invisible',
          '#default_value' => $block['weight'],
        );
      }
    }
  }

  if (empty($form['settings']['slide_push_menu_blocks'])) {
    $form['settings']['menu_blocks_instruction'] = array(
      '#type' => 'item',
      '#title' => t('Menu Blocks'),
      '#markup' => t('Currently no menu blocks have been added to the slide
        push menu. To add a menu block to this menu: <ul><li>Visit the block
        setting page for your menu block</li><li>Tick the slide push menu
        checkbox in the "slide push menu" vertical tab at the bottom of
        the page.</li></ul>'),
    );
  }



  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 9999,
  );

  return $form;
}

/**
 * set the settings value to variable table
 *
 * @param array $form
 *   Standard form array
 *
 * @param array $form_state
 *   Standard form_state array
 */
function slide_push_settings_form_submit($form, $form_state) {
  if (!empty($form_state['values']['settings']) && is_array($form_state['values']['settings'])) {
    foreach ($form_state['values']['settings'] as $field => $val) {
      $is_variable = strpos($field, 'slide_push_');
      if ($is_variable !== FALSE) {
        variable_set($field, $val);
      }
    }
  }
}

/**
 * Theme function for slide_push_settings_form.
 *
 * @param array $variables
 *   Standard theme variables
 */
function theme_slide_push_settings_form($variables) {
  $form = $variables['form'];

  if (!empty($form['settings']['slide_push_menu_blocks'])) {
    $rows = array();
    foreach (element_children($form['settings']['slide_push_menu_blocks']) as $bid) {
      $form['settings']['slide_push_menu_blocks'][$bid]['weight']['#attributes']['class'] = array('blocks-order-weight');
      $rows[] = array(
        'data' => array(
          array('class' => array('block-cross')),
          drupal_render($form['settings']['slide_push_menu_blocks'][$bid]['title']),
          drupal_render($form['settings']['slide_push_menu_blocks'][$bid]['weight']),
        ),
        'class' => array('draggable'),
      );
    }

    $form['settings']['slide_push_menu_blocks']['#description'] = t('To add a
      menu block to this menu: <ul><li>Visit the block setting page for your
      menu block</li><li>Tick the slide push menu checkbox in the "slide push
      menu" vertical tab at the bottom of the page.</li><li>Drag the icon next
      to the block name below to order the menus.</li></ul>');

    $header = array('', t('Block name'), t('Weight'));

    $form['settings']['slide_push_menu_blocks']['table'] = array(
      '#markup' => theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('id' => 'blocks-order'),
      )),
    );
  }
  $output .= drupal_render($form['note']);
  $output = drupal_render_children($form);

  drupal_add_tabledrag('blocks-order', 'order', 'sibling', 'blocks-order-weight');

  return $output;
}

/**
 * set push class
 *
 * @param array $form
 *   Standard form array
 *
 * @param array $form_state
 *   Standard form_state array
 *
 * @return array $form
 */
function slide_push_is_push_ajax_callback($form, $form_state) {
  $output = '';

  if ($form_state['values']['settings']['slide_push_is_push']) {
    $output = $form['settings']['slide_push_push_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Push Class'),
      '#default_value' => variable_get("slide_push_push_class", '#page'),
      '#description' => t('Please specify the class/id of the content, which will be pushed. e.g.: #page'),
      '#required' => TRUE,
      '#prefix' => '<div id="slidepush-push-class-wrapper">',
      '#suffix' => '</div>',
      '#access' => $form_state['values']['settings']['slide_push_is_push'],
    );
  }
  else {
    $output = $form['settings']['is_push_placeholder'] = array(
      '#type' => 'item',
      '#markup' => '<div id="slidepush-push-class-wrapper"></div>',
    );
  }

  return $output;

}

/**
 * compare weight of a to b
 *
 * @param array $a
 *   Standard array
 *
 * @param array $b
 *   Standard array
 */
function slide_push_cmp($a, $b) {
  if (empty($a['weight']) || empty($b['weight'])) {
    return 0;
  }

  $aw = $a['weight'];
  $bw = $b['weight'];

  if ($aw == $bw) {
    return 0;
  }
  return ($aw < $bw) ? -1 : 1;
}

/**
 * sort the menu blocks based on their weight
 */
function slide_push_sorted_menu_blocks() {
  $menu_blocks = variable_get("slide_push_menu_blocks", array());
  if (is_array($menu_blocks)) {
    usort($menu_blocks, "slide_push_cmp");
  }
  return $menu_blocks;
}

/**
 * send slid push setting value to javascript
 *
 * @param array $page
 *   Standard array
 */
function slide_push_page_alter(&$page) {
  $slide_push_js_settings = &drupal_static(__FUNCTION__);
  if (!isset($slide_push_js_settings)) {
    $settings = array(
      'side' => variable_get("slide_push_side", "left"),
      'is_push' => (bool) variable_get("slide_push_is_push", 0),
      'push_class' => variable_get("slide_push_push_class", '#page'),
      'menu_class' => variable_get("slide_push_menu_class", '#region-branding .branding-data'),
      'menu_width' => variable_get("slide_push_menu_width", '22.625em'),
      'menu_label' => variable_get("slide_push_menu_label", 'Menu'),
      'header_label' => variable_get("slide_push_header_label", 'Menu'),
      'close_label' => variable_get("slide_push_close_label", 'close'),
      'menu_blocks' => slide_push_sorted_menu_blocks(),
    );
    drupal_add_js(array('slidepush' => $settings), 'setting');
  }
}

/**
 * Impletement hook_form_block_admin_configure_alter
 *
 * @param array $form
 *   Standard form array
 *
 * @param array $form_state
 *   Standard form_state array
 *
 * @param string $form_id
 *   form_id
 */
function slide_push_form_block_admin_configure_alter(&$form, &$form_state, $form_id) {
  if (! (!empty($form['module']['#value']) && 'menu_block' == $form['module']['#value'])) {
    return FALSE;
  }

  $form['visibility']['slidepush'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide Push Menu'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => count($form['visibility']),
  );

  $form['visibility']['slidepush']['slide_push_add'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add this menu block to Slide Push menu'),
    '#default_value' => slide_push_menu_block_enabled($form['delta']['#value']),
    '#description' => t('If checked, this menu block will be added to Slide Push menu.'),
    '#required' => FALSE,
  );

  array_unshift($form['#submit'], 'slide_push_form_block_admin_configure_submit');

}


/**
 * check if the menu block is enabled
 *
 * @param string $delta
 *   delta of block
 */
function slide_push_menu_block_enabled($delta) {
  // $blocks[4] = array('bid' => 4, 'enabled' => 1, 'weight' => 0);
  $blocks = variable_get("slide_push_menu_blocks", array());

  return (!empty($blocks[$delta]['enabled'])) ? 1 : 0;
}


/**
 * save the menu block if it is enabled.
 *
 * @param array $form
 *   Standard form array
 *
 * @param array $form_state
 *   Standard form_state array
 */
function slide_push_form_block_admin_configure_submit(&$form, &$form_state) {
  $blocks = variable_get("slide_push_menu_blocks", array());

  $enabled = (!empty($form_state['values']['slide_push_add'])) ? 1 : 0;
  $weight = ($blocks[$form_state['values']['delta']]['weight']) ? $blocks[$form_state['values']['delta']]['weight'] : 0;

  $exists = FALSE;
  foreach ($blocks as $i => $block) {
    if (!empty($block['bid']) && $form_state['values']['delta'] == $block['bid']) {
      $exists = TRUE;
      $blocks[$i]['enabled'] = $enabled;
    }
  }

  if (!$exists) {
    $blocks[$form_state['values']['delta']] = array(
      'bid' => $form_state['values']['delta'],
      'enabled' => $enabled,
      'weight' => $weight,
    );
  }
  variable_set("slide_push_menu_blocks", $blocks);
}
