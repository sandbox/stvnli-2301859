(function ($) {
  Drupal.behaviors.slide_push = {
    attach: function (context, settings) {
        
      var opt = (opt === undefined && settings.slidepush) ? settings.slidepush : {};

      if (!opt) {return ; }

      $('body').once('spmenu-js',function(){
  			 var 
  			 	$spmenu_left = $("<nav>", {'id': "spmenu", 'class': "panel", 'role': "navigation"}),
  			 	$spmenu_header = $("<h2>", {'id': "spmenu-header"}),
  			 	$spmenu_btn = $("<button>", {'id': "show-push"}),
  			 	$spmenu_btn_close = $("<button>", {'id': "hide-push"})
  			 ;
  			 
  			 $spmenu_btn.text(opt.menu_label);
  			 $spmenu_header.text(opt.header_label);
  			 $spmenu_btn_close.text(opt.close_label);
  			 $spmenu_left.prepend($spmenu_header).prepend($spmenu_btn_close);
  			 
  			 
  			 
  			 // wrap block-menu-block-4 block-menu-block-5 to slide push menu
  			 if (opt.menu_blocks) {
    			 $.each(opt.menu_blocks, function( index, block ) {
    			   if (block.enabled) {
               $('#block-menu-block-' + block.bid).appendTo($spmenu_left);
             }
           });
         }
         
  			 $(this).append($spmenu_left);
  			 
  			 //prepend menu button here
  			 $(opt.menu_class).prepend($spmenu_btn);
  			 
  			 // Set the elements to push to right
  			 if (opt.is_push) {
  			   $(opt.push_class).addClass('push');
  			 }
  			 
  			 
  			 var $bs = $spmenu_btn.bigSlide({'menu': "#spmenu", 'side': opt.side, 'menuWidth': opt.menu_width});
  			 
  			 // Set additional close button
  			 $spmenu_btn_close.click(function(){$bs.close();});
  			 
       });
     
    } //-- end attach
  };
})(jQuery);